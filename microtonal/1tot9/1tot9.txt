# Intervallen met de cijfers 1 tm 9

1/1 300Hz
2/1 600Hz
3/2 450Hz
4/3 400Hz
5/4 375Hz
6/5 360Hz
7/6 350Hz
8/7 342.857Hz
9/8 337.5Hz

# nu op volgorde

1/1 300Hz
9/8 337.5Hz
8/7 342.857Hz
7/6 350Hz
6/5 360Hz
5/4 375Hz
4/3 400Hz
3/2 450Hz
2/1 600Hz

